const getSum = (str1, str2) => { 
  
 if(typeof(str1)!='string'||typeof(str2)!='string'){
  return false;
 }
 if (str1.length==0) return str2;
if (str2.length==0) return str1;
 if (Number.isNaN(Number.parseInt(str1))||Number.isNaN(Number.parseInt(str2))) {
  return false;
 }
 let sum =Number.parseInt(str1)+Number.parseInt(str2);

  return String(sum);
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let post=0;
  let coment=0;
  for (const n of listOfPosts) {
    if (n.author==authorName) {
      post++;
    }
    if ('comments'in n) {
      for (let i=0;i<n.comments.length;i++  ) {
        if (n.comments[i].author==authorName) {
          coment++;
        }
      }
    }
  }
  
  
  return `Post:${post},comments:${coment}`;
};

const tickets=(people)=> {
  let vasia=0;
  for (let index = 0; index < people.length; index++) {
   
   if ((people[index]-25)>vasia) {
    return 'NO';
   } 
   vasia+=25;
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
